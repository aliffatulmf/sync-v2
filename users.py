from model import database, Users, APIName
from uuid import uuid4
import requests
import sys
import hashlib


def get_session(username, password, **kwargs):
    """
    Returning user session to HTTP response.
    if UUID is empty then it will throw error

    Function:
    get_session(username, password, **kwargs) -> Response

    Keyword arguments:
    username -- user username (default=None)
    password -- user password (default=None)
    kwargs.key -- UUID (default=None)
    """
    if kwargs["key"] is None:
        raise ValueError("UUID Required")

    url = APIName.get(APIName.name == "Sessions").url
    data = {
        "username": username,
        "password": password,
        "device_id": kwargs["key"]
    }
    headers = {"content-type": "application/json"}

    response = requests.post(url, json=data, headers=headers)

    return response


def get_user_data(username, password, **kwargs):
    """
    Returning user data to HTTP response.
    if UUID is empty then it will throw error

    Function:
    get_user_data(username, password, **kwargs) -> Response

    Keyword arguments:
    username -- user username (default=None)
    password -- user password (default=None)
    kwargs.key -- UUID (default=None)
    """
    if kwargs["key"] is None:
        raise ValueError("UUID Required")

    url = APIName.get(APIName.name == "Login").url
    data = "-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"username\"\r\n\r\n{username}\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"password\"\r\n\r\n{password}\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"captcha\"\r\n\r\n\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"captcha_key\"\r\n\r\n\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"remember\"\r\n\r\n{remember}\r\n-----011000010111000001101001--\r\n".format(
        username=username, password=password, remember=False)

    headers = {
        "Content-Type":
        "multipart/form-data; boundary=---011000010111000001101001"
    }
    querystring = {"SPC_CDS": kwargs["key"], "SPC_CDS_VER": 2}  # URL Parameter

    response = requests.post(url,
                             data=data,
                             headers=headers,
                             params=querystring)

    return response


def input_user(username, password):
    """
    Process the search and retrieval of user data and save it to the database.
    """
    UUID = str(uuid4())

    user = get_user_data(username=username, password=password, key=UUID)

    if user.status_code != 200:
        return sys.exit("\nUser not found")

    session = get_session(username=username, password=password, key=UUID)

    if session.status_code != 200:
        return sys.exit("\nSessions not found")

    user_json = user.json()
    session_json = session.json()
    sec_passwd = hashlib.sha1(bytes(password, "utf-8")).hexdigest()
    try:
        query = Users.insert({
            "user_id": user_json["id"],
            "username": username,
            "password": sec_passwd,
            "shop_name": None,
            "shop_id": user_json["shopid"],
            "token": session_json["token"],
            "email": user_json["email"],
            "uuid": UUID,
            "status": True
        })

        with database.atomic():
            query.execute()
            print("\nSave to database")
    except Exception:
        raise Exception("Error user input")

    return 1


def delete_user(username):
    """
    Deleting a user session and changing the account status to False to the database
    """
    try:
        USER = Users.select().where(Users.username == username, Users.status)

        for usr in USER:
            print("User {username} found".format(username=usr.username))

            url = APIName.get(APIName.name == "Delete").url
            headers = {
                "Content-Type": "application/json",
                "Authorization": "Bearer {token}".format(token=usr.token)
            }

            response = requests.delete(url, headers=headers)

            if response.status_code == 204:
                print("User deleted")
            elif response.status_code == 401:
                print("User has been deleted")

            with database.atomic():
                usr.update({"status": False}).execute()

    except ValueError:
        print("\nOops! Users not found")


def list_user():
    """List of users where the session is still alive"""

    search = Users.select(Users.username, Users.status).where(
        Users.status).order_by(Users.username.asc())
    if not search:
        sys.exit("Users not found")

    print("Username\t\tStatus")
    print(50 * "-")
    for s in search:
        print("%s\t\t%s" % (s.username, bool(s.status)))
